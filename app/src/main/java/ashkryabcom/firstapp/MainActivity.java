package ashkryabcom.firstapp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


import static ashkryabcom.firstapp.R.layout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    static View[][] cells;
    static Button buttonStart;
    static Button buttonClear;
    static Button buttonSettings;
    static TextView cellsCountView;
    static LinearLayout switchCellSizeView;
    int sizeOfCell = 30;
    TableLayout table;
    RadioButton rbSmall;
    RadioButton rbMiddle;
    RadioButton rbLarge;

    //TODO gen counter

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.main);
        table = (TableLayout) findViewById(R.id.table);
        switchCellSizeView = (LinearLayout) findViewById(R.id.SwitchCellSizeView);
        switchCellSizeView.setVisibility(View.INVISIBLE);

        cellsCountView = (TextView) findViewById(R.id.textView_CellsCount);

        buttonStart = (Button) findViewById(R.id.buttonStart);
        buttonStart.setOnClickListener(new Listeners.BtnStart());
        buttonClear = (Button) findViewById(R.id.buttonClear);
        buttonClear.setOnClickListener(new Listeners.BtnClear());
        buttonSettings = (Button) findViewById(R.id.buttonSettings);
        buttonSettings.setOnClickListener(new Listeners.BtnSettings());

        rbSmall = (RadioButton) findViewById(R.id.radioButton_Small);
        rbSmall.setOnClickListener(this);
        rbMiddle = (RadioButton) findViewById(R.id.radioButton_Middle);
        rbMiddle.setOnClickListener(this);
        rbMiddle.setChecked(true);
        rbLarge = (RadioButton) findViewById(R.id.radioButton_Large);
        rbLarge.setOnClickListener(this);


        // get height and width in new thread and draw field
        // cell 20x20 dp
        table.post(new Runnable() {
            @Override
            public void run() {
                int h = table.getHeight() / sizeOfCell;
                int w = table.getWidth() / sizeOfCell;
                drawGrid(h, w, table, sizeOfCell);
                new BtnTasks(h, w, cells);
            }
        });
    }


    void drawGrid(int h, int w, TableLayout table, int sizeOfCell) {
        table.removeAllViews();
        TableRow[] rows = new TableRow[h];
        cells = new View[h][w];
        for (int r = 0; r < rows.length; r++) {
            rows[r] = new TableRow(MainActivity.this);
            table.addView(rows[r]);
            for (int j = 0; j < cells[r].length; j++) {
                cells[r][j] = new View(MainActivity.this);
                cells[r][j].setOnClickListener(new Listeners.CellListener());
                cells[r][j].setBackgroundColor(Color.WHITE);
                rows[r].addView(cells[r][j], sizeOfCell, sizeOfCell);
            }
        }
    }

    public void onClick(View v) {

        RadioButton rb = (RadioButton) v;
        switch (rb.getId()) {
            case R.id.radioButton_Small:
                if (!rb.isActivated()) {
                    rbMiddle.setChecked(false);
                    rbLarge.setChecked(false);
                    sizeOfCell = 20;
                }
                break;
            case R.id.radioButton_Middle:
                if (!rb.isActivated()) {
                    rbSmall.setChecked(false);
                    rbLarge.setChecked(false);
                    sizeOfCell = 35;
                }
                break;
            case R.id.radioButton_Large:
                if (!rb.isActivated()) {
                    rbSmall.setChecked(false);
                    rbMiddle.setChecked(false);
                    sizeOfCell = 45;
                }
                break;
        }

        int h = table.getHeight() / sizeOfCell;
        int w = table.getWidth() / sizeOfCell;
        drawGrid(h, w, table, sizeOfCell);
        BtnTasks.h = h;
        BtnTasks.w = w;
        BtnTasks.cells = cells;
    }
}
