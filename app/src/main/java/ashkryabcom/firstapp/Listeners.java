package ashkryabcom.firstapp;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;


class Listeners {
    private static BtnTasks.DoGame game;

    static class BtnSettings implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            View switchGroup = MainActivity.switchCellSizeView;
            if (switchGroup.getVisibility() == View.INVISIBLE) {
                switchGroup.setVisibility(View.VISIBLE);
            } else {
                switchGroup.setVisibility(View.INVISIBLE);
            }
        }
    }


    static class BtnStart implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (game == null || game.isCancelled()) {
                game = new BtnTasks.DoGame();
                game.execute();
                MainActivity.buttonStart.setVisibility(View.INVISIBLE);
                MainActivity.buttonClear.setVisibility(View.INVISIBLE);
                MainActivity.buttonSettings.setVisibility(View.INVISIBLE);
                MainActivity.switchCellSizeView.setVisibility(View.INVISIBLE);
            }
        }
    }

    static class BtnClear implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            new BtnTasks.Clear().execute();
        }
    }

    //TODO replace onClick to onTouch, implement drag and drop
    static class CellListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (game != null) {
                game.cancel(true);
            }
            if (MainActivity.buttonStart.getVisibility() != View.VISIBLE) {
                MainActivity.buttonStart.setVisibility(View.VISIBLE);
                MainActivity.buttonClear.setVisibility(View.VISIBLE);
                MainActivity.buttonSettings.setVisibility(View.VISIBLE);

            } else {
                if (((ColorDrawable) v.getBackground()).getColor() == Color.GRAY) {
                    v.setBackgroundColor(Color.WHITE);
                } else {
                    v.setBackgroundColor(Color.GRAY);
                }
            }
        }
    }
}
