package ashkryabcom.firstapp;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.View;

import java.util.concurrent.TimeUnit;

final class BtnTasks {

    private static boolean[][] people;
     static int h, w;
     static View[][] cells;

    BtnTasks(int h, int w, View[][] cells) {
        BtnTasks.h = h;
        BtnTasks.w = w;
        BtnTasks.cells = cells;
        people = new boolean[h][w];
    }

    static class Clear extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            people = new boolean[h][w];
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            for (View[] cellRow : cells) {
                for (View cell : cellRow) {
                    cell.setBackgroundColor(Color.WHITE);
                }
            }
        }
    }

    static class DoGame extends AsyncTask<Void, Void, Void> {

        int[][] checkXY = new int[][]{
                {-1, 0, 1, -1, 1, -1, 0, 1},
                {-1, -1, -1, 0, 0, 1, 1, 1}};
        boolean[][] temp;

        void insertPeople() {
            for (int i = 0; i < h; i++) {
                for (int j = 0; j < w; j++) {
                    people[i][j] = ((ColorDrawable) cells[i][j].getBackground()).getColor() == Color.GRAY;
                }
            }
        }

        //TODO infinity field
        void doNextGen() {
            temp = new boolean[people.length][people[0].length];
            for (int i = 0; i < h; i++) {
                for (int j = 0; j < w; j++) {
                    if (check(i, j) < 2 || check(i, j) > 3) {
                        temp[i][j] = false;
                    } else if (check(i, j) == 2) {
                        temp[i][j] = people[i][j];
                    } else if (check(i, j) == 3)
                        temp[i][j] = true;
                }
            }
            people = temp;
        }

        int check(int y, int x) {
            int count = 0;
            for (int i = 0; i < 8; i++) {
                try {
                    if (people[y + checkXY[1][i]][x + checkXY[0][i]]) {
                        count++;
                    }
                } catch (Exception ignored) {

                }
            }
            return count;
        }

        @Override
        protected Void doInBackground(Void... params) {
            insertPeople();
            while (!isCancelled()) {
                doNextGen();

                publishProgress();
                try {
                    TimeUnit.MILLISECONDS.sleep(1000 / 5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... v) {
            super.onProgressUpdate(v);
            int cellsCount=0;

            for (int i = 0; i < cells.length; i++) {
                for (int j = 0; j < cells[i].length; j++) {
                    if (people[i][j]) {
                        cells[i][j].setBackgroundColor(Color.GRAY);
                        cellsCount++;
                    } else {
                        cells[i][j].setBackgroundColor(Color.WHITE);
                    }
                }
            }
            MainActivity.cellsCountView.setText("Count: "+cellsCount);
        }
    }
}